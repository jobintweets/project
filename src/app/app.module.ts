import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProfilesettingsPage } from '../pages/profilesettings/profilesettings';
import{MywalletPage} from '../pages/mywallet/mywallet';
import { IonPullupModule } from 'ionic-pullup';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ProfilesettingsPage,
    MywalletPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonPullupModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ProfilesettingsPage,
    MywalletPage

    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ] 
})
export class AppModule {}
