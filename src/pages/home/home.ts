import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IonPullUpFooterState, IonPullUpComponent } from 'ionic-pullup';
// @input() footer: IonPullUpComponent; 
import { AlertController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  name: string;
  email:string;
  phone:string;
  address:string;

  footerState: IonPullUpFooterState;
  constructor(public navCtrl: NavController,private alertCtrl: AlertController) {
    this.footerState = IonPullUpFooterState.Collapsed;

  }
  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }
  getMaximumHeight() {
    return window.innerHeight / 2;
}
showPrompt(name,email,phone,address) {
  let prompt = this.alertCtrl.create({
    title: 'Edit Profile',
   
    inputs: [
      {
        name: 'name',
       placeholder: 'Name',
       value:this.name
      },
      {
        name: 'email',
       placeholder: 'Email',
       value:this.email
      },
      {
        name: 'phone',
       placeholder: 'Phone',
       value:this.phone
      },
      {
        name: 'address',
       placeholder: 'Address',
       value:this.address
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Submitt',
        handler: data => {
          console.log(data);
          this.name = data.name;
          this.email=data.email;
          this.phone=data.phone;
          this.address=data.address;
          // this.name=data.title1;
        }
      }
    ]
  });
  
  prompt.present();
}
// showPrompt1() {
//   let prompt = this.alertCtrl.create({
//     title: 'Email',
   
//     inputs: [
//       {
//         name:'title1',
       
//         placeholder: 'Email'
//       },
//     ],
//     buttons: [
//       {
//         text: 'Cancel',
//         handler: data => {
//           console.log('Cancel clicked');
//         }
//       },
//       {
//         text: 'Submitt',
//         handler: data => {
//           console.log(data);
//           this.name = data.title1;
//         }
//       }
//     ]
//   });
//   // this.name=
//   prompt.present();
// }


}








