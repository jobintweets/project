webpackJsonp([2],{

/***/ 108:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 108;

/***/ }),

/***/ 149:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/mywallet/mywallet.module": [
		276,
		1
	],
	"../pages/profilesettings/profilesettings.module": [
		277,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 149;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// @input() footer: IonPullUpComponent; 

var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.footerState = __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    }
    HomePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    HomePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    HomePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    HomePage.prototype.getMaximumHeight = function () {
        return window.innerHeight / 2;
    };
    HomePage.prototype.showPrompt = function (name, email, phone, address) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Edit Profile',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Name',
                    value: this.name
                },
                {
                    name: 'email',
                    placeholder: 'Email',
                    value: this.email
                },
                {
                    name: 'phone',
                    placeholder: 'Phone',
                    value: this.phone
                },
                {
                    name: 'address',
                    placeholder: 'Address',
                    value: this.address
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Submitt',
                    handler: function (data) {
                        console.log(data);
                        _this.name = data.name;
                        _this.email = data.email;
                        _this.phone = data.phone;
                        _this.address = data.address;
                        // this.name=data.title1;
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/ghost_man12/jobin/src/pages/home/home.html"*/'\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding style="background-color: #dedede">\n  \n      <div id="test">\n    \n        <ion-list no-lines >\n        \n            <ion-row>\n                \n            <ion-item style="border-bottom:1px solid red;background-color:transparent;color:black; " >\n                <!-- <ion-input placeholder="Brain Oconner"  [(ngModel)]="name" disabled="true"></ion-input> -->\n                <a>{{name}}</a>\n               \n                   <ion-icon item-right  style="margin-right: 0px;" on-click="showPrompt(name)" ><ion-img src="../../assets/imgs/edit.png" ></ion-img></ion-icon>\n               </ion-item> \n              </ion-row>\n            \n              <ion-item style="border-bottom:1px solid red;background-color:transparent;color:black; ">\n                <!-- <ion-input placeholder="brain@gmail.com"  [(ngModel)]="email" disabled="true"></ion-input> -->\n                <a>{{email}}</a>\n                \n                   <ion-icon item-right  style="margin-right: 0px;" on-click="showPrompt(email)"><ion-img src="../../assets/imgs/edit1.png"></ion-img></ion-icon>\n\n              </ion-item>\n                   <ion-item  style="border-bottom:1px solid red;background-color:transparent;color:black; " >\n                      <!-- <ion-input   placeholder="077-666-555"  [(ngModel)]="phone" disabled="true"></ion-input> -->\n                      <a>{{phone}}</a>\n                     \n                         <ion-icon item-right  style="margin-right: 0px;" on-click="showPrompt(phone)"><ion-img src="../../assets/imgs/edit1.png"></ion-img></ion-icon>\n\n                   \n                   </ion-item>\n              \n                   <div >\n                   <ion-item  id="address" style="background-color:transparent;color:black;">\n                      <!-- <ion-input  placeholder="some address" [(ngModel)]="address" disabled="true"></ion-input> -->\n                      <a>{{address}}</a>\n                     \n                     <ion-icon item-right  style="margin-right: 0px;" on-click=" showPrompt(address)"><ion-img src="../../assets/imgs/edit1.png"></ion-img></ion-icon>\n\n                   </ion-item>\n                  </div>\n                  \n                   \n                   \n                    \n           \n          </ion-list>\n          </div>\n          \n        \n</ion-content>\n\n<!-- pullup footer starts from here --><ion-footer >\n<ion-pullup   (onExpand)="footerExpanded()" \n(onCollapse)="footerCollapsed()" [(state)]="footerState" style="background-color:#488aff;" [maxHeight]="getMaximumHeight()">\n  <ion-pullup-tab   (tap)="toggleFooter()" >\n    <ion-icon  name="arrow-up" *ngIf="footerState == 0">\n    </ion-icon><ion-icon name="arrow-down" *ngIf="footerState == 1"></ion-icon>\n  </ion-pullup-tab >\n  <ion-toolbar color="primary" (tap)="toggleFooter()">\n    <ion-title>Change Password</ion-title>\n   \n  </ion-toolbar>\n  <ion-content padding   style="background-color:#488aff;">\n    <form (ngSubmit)="onSubmit(form)" #form="ngForm"  >\n    \n      <ion-list style="margin:auto" no-lines >\n        <ion-row no-padding no-margin>\n          <ion-item id="rounded" >\n              <ion-input class="form-control"  name="password" type="password" required ngModel placeholder="Current password"></ion-input>\n              <!-- <ion-label floating>Current password</ion-label> -->\n            </ion-item>\n          </ion-row> \n            <ion-item id="rounded">\n              <ion-input class="form-control"  name="password" type="password" required ngModel placeholder="New password"></ion-input>\n              <!-- <ion-label floating>New password</ion-label> -->\n            </ion-item>\n                 <ion-item id="rounded">\n                    <ion-input class="form-control"  name="password" type="password" required ngModel placeholder="Confirm password"></ion-input>\n                    <!-- <ion-label floating >Confirm password</ion-label> -->\n                 </ion-item>\n                 \n                 \n                  \n         \n        </ion-list>\n    <div id="button1">\n      <button ion-button color="light" small  type="submit" margin >Change password</button> \n\n    </div>\n   \n            \n        </form> \n     \n  </ion-content>\n\n</ion-pullup>\n\n</ion-footer>'/*ion-inline-end:"/home/ghost_man12/jobin/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _b || Object])
    ], HomePage);
    return HomePage;
    var _a, _b;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/home/ghost_man12/jobin/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/ghost_man12/jobin/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MywalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MywalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MywalletPage = /** @class */ (function () {
    function MywalletPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
    }
    MywalletPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MywalletPage');
    };
    MywalletPage.prototype.showPrompt = function () {
        var prompt = this.alertCtrl.create({
            title: 'Amount',
            message: "Enter the amount in $",
            inputs: [
                {
                    name: 'title',
                    placeholder: 'Amount'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Submitt',
                    handler: function (data) {
                        console.log('Saved clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    MywalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-mywallet',template:/*ion-inline-start:"/home/ghost_man12/jobin/src/pages/mywallet/mywallet.html"*/'<!--\n  Generated template for the MywalletPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n      <!-- <ion-img src="../../assets/imgs/refresh.png" width="10" height="10"></ion-img> -->\n\n    <ion-title>My Wallet</ion-title>\n    <ion-buttons end>\n        <button ion-button icon-only color="royal">\n          <ion-icon name="refresh"></ion-icon>\n        </button>\n      </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n  <div id="redcolor" span>\n\n<div class="circle">\n  <!-- <p>$440</p> -->\n</div>\n  </div>\n  \n<form   #form="ngForm" padding    style=" background-color: #80808040;">\n\n  \n  <button ion-button full  block color="light" on-click=" showPrompt()" style="background-color:white;">\n    Enter Amount\n    </button>\n\n<ion-item id="item" no-padding >\n  <ion-input placeholder="Card number" type="text"></ion-input>\n</ion-item>\n<ion-item  id="item" no-padding >\n  <ion-input placeholder="Name in the card" type="text"></ion-input>\n</ion-item>\n\n  <ion-row >\n    <ion-col col-5 placeholder="expiry month">\n      <ion-item   no-lines no-padding>\n        <ion-label>expiry month</ion-label>\n        <ion-select >\n          <ion-option>Jan</ion-option>\n          <ion-option>Feb</ion-option>\n          <ion-option>Mar</ion-option>\n          <ion-option>Apr</ion-option>\n          <ion-option>May</ion-option>\n          <ion-option>Jun</ion-option>\n          <ion-option>Jul</ion-option>\n          <ion-option>Aug</ion-option>\n          <ion-option>Sep</ion-option>\n          <ion-option>Oct</ion-option>\n          <ion-option>Nov</ion-option>\n          <ion-option>Dec</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n    <ion-col col-1></ion-col>\n    <ion-col col-5  >\n      <ion-item   no-padding>\n        <ion-label>expiry year</ion-label>\n        <ion-select>\n          <ion-option>2018</ion-option>\n          <ion-option>2019</ion-option>\n          <ion-option>2020</ion-option>\n          <ion-option>2021</ion-option>\n          <ion-option>2022</ion-option>\n          <ion-option>2023</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n    <!-- <ion-col col-4></ion-col> -->\n  </ion-row>\n  <ion-row style="padding-left:5px;">\n\n  <ion-item style="width:36%;"  >\n      \n\n    <ion-input type="text" value=""  placeholder="CCV" maxlength="3" ></ion-input>\n    <ion-icon item-right name="card"></ion-icon>\n  </ion-item>\n</ion-row>\n\n  <ion-row   >\n      \n    <!-- <ion-col col-4> -->\n    <ion-checkbox margin-left ></ion-checkbox>\n    <a >save this card</a>\n  <!-- </ion-col> -->\n  <!-- <ion-col col-1></ion-col> -->\n\n  </ion-row>\n\n\n</form>\n \n  \n  \n  <ion-footer no-padding>\n    <ion-toolbar color="primary">\n      <ion-title>Add Amount</ion-title>\n    </ion-toolbar>\n  </ion-footer>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/ghost_man12/jobin/src/pages/mywallet/mywallet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MywalletPage);
    return MywalletPage;
}());

//# sourceMappingURL=mywallet.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilesettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ProfilesettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilesettingsPage = /** @class */ (function () {
    function ProfilesettingsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProfilesettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilesettingsPage');
    };
    ProfilesettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-profilesettings',template:/*ion-inline-start:"/home/ghost_man12/jobin/src/pages/profilesettings/profilesettings.html"*/'<!--\n  Generated template for the ProfilesettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title></ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/ghost_man12/jobin/src/pages/profilesettings/profilesettings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ProfilesettingsPage);
    return ProfilesettingsPage;
}());

//# sourceMappingURL=profilesettings.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(223);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_profilesettings_profilesettings__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_mywallet_mywallet__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ionic_pullup__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_profilesettings_profilesettings__["a" /* ProfilesettingsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_mywallet_mywallet__["a" /* MywalletPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/mywallet/mywallet.module#MywalletPageModule', name: 'MywalletPage', segment: 'mywallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profilesettings/profilesettings.module#ProfilesettingsPageModule', name: 'ProfilesettingsPage', segment: 'profilesettings', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10_ionic_pullup__["b" /* IonPullupModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_profilesettings_profilesettings__["a" /* ProfilesettingsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_mywallet_mywallet__["a" /* MywalletPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
            // schemas: [ CUSTOM_ELEMENTS_SCHEMA ] 
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/ghost_man12/jobin/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/ghost_man12/jobin/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map